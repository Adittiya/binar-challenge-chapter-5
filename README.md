# Binar Academy Full-Stack Web Developer Challange 5

#### Preview

![Project](/public/images/preview.gif)

#### Goals

1. Move the challenge code in chapters 3 and 4 which was originally static HTML into the server using Express, page 1 with other pages distinguished by routing
2. Create static user data for login in the backend part
3. Using Postman to check the API 4. Serving static user data to JSON form

## Installation

You need to install Node.js and NPM

How To Use

- Clone or download Repository
- Open Directory using hyper, ubuntu, git bash, or terminal
- Install package with Run `npm i`
- Run server with `npm start` or `npm run dev`
- Open `http://localhost:3000` and other route in your browser

## Package Used

- Express.js
- body-parser
- fs
- ejs
- express-session
