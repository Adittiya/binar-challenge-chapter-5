"use strict";

// costumize scroll bar and add indicator bar 
$(".carousel").on("slid.bs.carousel", function () {
  $(".carousel-control-prev, .carousel-control-next").animate(
    {
      opacity: 1,
    },
    500
  );
});

$(".carousel-control-prev, .carousel-control-next").on("click", function () {
  $(".carousel-control-prev, .carousel-control-next").animate(
    {
      opacity: 0,
    },
    100
  );
});
$(window).on("scroll", function () {
  const indicatorBar = $(".scroll-indicator-bar");

  const pageScroll = $(document).scrollTop();
  const height = $(document).height() - $(window).height();
  const scrollValue = (pageScroll / height) * 100;

  indicatorBar.width(scrollValue + "%");
});

const menuBtn = $(".nav-menu-btn");
const closeBtn = $(".nav-close-btn");
const navigation = $(".navigation");

menuBtn.on("click", function () {
  navigation.addClass("active");
});

closeBtn.on("click", function () {
  navigation.removeClass("active");
});

// change navbar color when user in section other main section
const navbar = $(".navbar");
const main = $("#main");

$(window).scroll(function() {
  if (main.offset().top + main.outerHeight() > $(this).scrollTop()) {
    navbar.removeClass("black-background");
  } else {
    navbar.addClass("black-background");
  }
}); 