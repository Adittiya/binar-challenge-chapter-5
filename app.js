"use strict";

import express from "express";
import bodyParser from "body-parser";
import ejs from "ejs";
import users from "./public/js/user.js";
import fs from "fs";
import session from "express-session";

const app = express();
const port = 3000;
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(
  session({
    secret: "secret-key",
    resave: false,
    saveUninitialized: false,
  })
);

app.get("/", (req, res) => {
  res.render("home", { user: req.session.user });
});


app.get("/play", (req, res) => {
  if (req.session.loggedIn) {
    res.render("play", { user: req.session.user });
  } else {
    res.redirect("/login");
  }
});


app.get("/register", (req, res) => {
  res.render("register");
});

app.get("/login", (req, res) => {
  res.render("login");
});

app.post("/register", (req, res) => {
  const nickRegister = req.body.nick;
  const emailRegister = req.body.email;
  const passwordRegister = req.body.password;

  const userExists = users.find((user) => user.email === emailRegister);
  if (userExists) {
    return res.send({ status: "error", message: "Email sudah terpakai" });
  }

  const user = {
    nick: nickRegister,
    email: emailRegister,
    password: passwordRegister,
  };

  users.push(user);

  fs.writeFile(
    "./public/js/user.js",
    "export default " + JSON.stringify(users),
    (err) => {
      if (err) {
        throw err;
      } else {
        console.log("Data berhasil disimpan");
        res.send({
          status: "success",
          message: "Register berhasil, Data disimpan di user.js",
        });
      }
    }
  );
});

app.post("/login", (req, res) => {
  const emailLogin = req.body.email;
  const passwordLogin = req.body.password;

  const user = users.find(
    (user) => user.email === emailLogin && user.password === passwordLogin
  );

  if (user) {
    req.session.loggedIn = true;
    req.session.user = user;
    // res.send({ status: "success", message: "Login berhasil!" });
    console.log("Login Berhasil")
    res.render("home", { user: req.session.user });
  } else {
    req.session.loggedIn = false;
    res.send({ status: "error", message: "Username atau password salah!" });
  }
});

// route untuk melihat list user 
app.get("/users", (req, res) => {
  const usersWithoutPassword = users.map((user) => {
    return {
      nick: user.nick,
      email: user.email,
    };
  });
  res.send({ users: usersWithoutPassword });
});

// route logout
app.get("/logout", (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      res.send({ status: "error", message: "Logout gagal" });
    } else {
      res.redirect("/");
    }
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
